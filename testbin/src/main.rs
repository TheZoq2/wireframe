extern crate ggez;

use ggez::event;
use ggez::graphics::{self, Color, DrawParam};
use ggez::{Context, GameResult};

use linedrawer::{SlopeLine, Line3d, Line2d, LineAxis};

use nalgebra as na;
use nalgebra::{Perspective3, Point3};
use ggez::nalgebra::Point2;


fn sloped_to_start_end(line: &SlopeLine) -> Line2d {
    let start = line.start;
    let end = Point2::new(start.x + line.length_along_primary, start.y + line.length_along_primary * line.slope);
    match line.axis {
        LineAxis::XAxis => Line2d::new(start, end),
        LineAxis::YAxis => Line2d::new(Point2::new(start.y, start.x), Point2::new(end.y, end.x))
    }
}


// First we make a structure to contain the game's state
struct MainState {
    frames: usize,
    mesh: Vec<Line3d>,
    perspective: Perspective3<f32>,
    time: f32
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let mesh = vec!{
            // Base
            Line3d::new(Point3::new(  0.,  0.2,  0.), Point3::new( 0.2,  0.2, -1.)),
            Line3d::new(Point3::new(  0.,  0.2,  0.), Point3::new( 0.0,  0.2, -1.)),
            // Line3d::new(Point3::new(-1.,  1.,  1.), Point3::new(-1., -1.,  -0.1)),
            // Line3d::new(Point3::new( 1.,  1.,  1.), Point3::new( 1., -1.,  -0.1)),
            // Line3d::new(Point3::new( 1., -1., -1.), Point3::new(-1., -1., -1.)),
            // Line3d::new(Point3::new(-1., -1., -1.), Point3::new(-1., -1.,  1.)),
            // Line3d::new(Point3::new(-1., -1.,  1.), Point3::new( 1., -1.,  1.)),
            // // Peak                         ,
            // Line3d::new(Point3::new( 1., -1.,  1.), Point3::new(0., 1., 0.)),
            // Line3d::new(Point3::new( 1., -1., -1.), Point3::new(0., 1., 0.)),
            // Line3d::new(Point3::new(-1., -1., -1.), Point3::new(0., 1., 0.)),
            // Line3d::new(Point3::new(-1., -1.,  1.), Point3::new(0., 1., 0.)),
        };

        let perspective = Perspective3::new(4./3., std::f32::consts::PI / 2., 0.01, 10.);

        let s = MainState {
            mesh,
            frames: 0,
            perspective,
            time: 0.
        };
        Ok(s)
    }
}


impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult {
        self.time += 0.01;
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, [0.1, 0.2, 0.3, 1.0].into());

        let mut mb = graphics::MeshBuilder::new();

        let rot = na::Matrix4::from_euler_angles(0., self.time, 0.);
        // let rot = na::Matrix4::from_euler_angles(0., 0., 0.);

        let lines = self.mesh
            .iter()
            .map(|l| l.transform(na::Transform3::from_matrix_unchecked(rot)))
            .filter_map(|l| l.project(self.perspective))
            .map(|l| l.to_sloped())
            .map(|l| sloped_to_start_end(&l))
            .collect::<Vec<_>>();

        if !lines.is_empty() {
            for line in lines {
                let on_screen = [
                    Point2::new(line.start.x * 800. + 400., line.start.y * 600. + 300.),
                    Point2::new(line.end.x * 800. + 400., line.end.y * 600. + 300.)
                ];

                mb.line(
                    &on_screen,
                    2.0,
                    Color::new(1.0, 1.0, 1.0, 1.0)
                )?;
            }

            let mesh = &mb.build(ctx)?;
            graphics::draw(ctx, mesh, DrawParam::default())?;
        }

        graphics::present(ctx)?;

        self.frames += 1;
        if (self.frames % 100) == 0 {
            println!("FPS: {}", ggez::timer::fps(ctx));
        }

        Ok(())
    }
}

pub fn main() -> GameResult {

    let cb = ggez::ContextBuilder::new("helloworld", "ggez");
    let (ctx, event_loop) = &mut cb.build()?;

    println!("HIDPI: {}", graphics::hidpi_factor(ctx));

    let state = &mut MainState::new(ctx)?;
    event::run(ctx, event_loop, state)
}
