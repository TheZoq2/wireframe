#![no_std]

use nalgebra as na;
use na::{Point3, geometry::Perspective3, Vector3, Point2};

use libm::F32Ext;



#[derive(Debug, Clone)]
pub struct Line3d {
    pub start: Point3<f32>,
    pub end: Point3<f32>,
}

impl Line3d {
    pub fn new(start: Point3<f32>, end: Point3<f32>) -> Self {
        Self {
            start,
            end
        }
    }

    pub fn transform(&self, transformation: na::Transform3<f32>) -> Line3d {
        Self {
            start: transformation.transform_point(&self.start),
            end: transformation.transform_point(&self.end),
        }
    }
}


impl Line3d {
    // TODO: Pass near clip here
    pub fn project(&self, perspective: Perspective3<f32>) -> Option<Line2d> {
        let (start, end) = clip_line_to_plane(
            (self.start, self.end),
            Vector3::new(0., 0., -1.),
            Point3::new(0., 0., -0.01)
        )?;

        let projected_start = perspective.project_point(&start);
        let projected_end = perspective.project_point(&end);

        Some(Line2d {
            start: Point2::new(projected_start.x, projected_start.y),
            end: Point2::new(projected_end.x, projected_end.y)
        })
    }
}


pub fn plane_distance(
    point: Point3<f32>,
    plane_normal: Vector3<f32>,
    plane_position: Point3<f32>
) -> f32 {
    (point.coords - plane_position.coords).dot(&plane_normal)
}

pub fn clip_line_to_plane(
    (mut start, mut end): (Point3<f32>, Point3<f32>),
    normal: Vector3<f32>,
    pos: Point3<f32>
) -> Option<(Point3<f32>, Point3<f32>)> {
    match (plane_distance(start, normal, pos), plane_distance(end, normal, pos)) {
        // Line is completely above plane
        (s, e) if s >= 0. && e >= 0. => Some((start, end)),
        // Line is completely above plane
        (s, e) if s < 0. && e < 0. => None,
        (s, e) if s < 0. && e >= 0. => {
            let scale = s / (s-e);
            start.x = start.x + scale * (end.x - start.x);
            start.y = start.y + scale * (end.y - start.y);
            start.z = start.z + scale * (end.z - start.z);
            Some((start, end))
        }
        (s, e) => {
            let scale = s / (s-e);
            end.x = start.x + scale * (end.x - start.x);
            end.y = start.y + scale * (end.y - start.y);
            end.z = start.z + scale * (end.z - start.z);
            Some((start, end))
        }
    }
}


#[derive(Debug, Clone, PartialEq)]
pub struct Line2d {
    pub start: Point2<f32>,
    pub end: Point2<f32>
}

impl Line2d {
    pub fn new(start: Point2<f32>, end: Point2<f32>) -> Self {
        Self {start, end}
    }

    pub fn to_sloped(&self) -> SlopeLine {
        let mut start = self.start;
        let mut end = self.end;

        let mut x_length = end.x - start.x;
        let mut y_length = end.y - start.y;

        // If the line has a slope larger than 1
        let axis = if x_length.abs() < y_length.abs() {
            let y = start.y;
            start.y = core::mem::replace(&mut start.x, y);

            let y = end.y;
            end.y = core::mem::replace(&mut end.x, y);

            core::mem::swap(&mut x_length, &mut y_length);
            LineAxis::YAxis
        }
        else {
            LineAxis::XAxis
        };

        // Ensure that the start point has the smallest value along the primary axis
        if x_length < 0. {
            core::mem::swap(&mut start, &mut end);
        }

        let slope = y_length / x_length;

        SlopeLine {
            start,
            slope,
            length_along_primary: x_length.abs(),
            axis
        }
    }
}


#[derive(Debug, Clone, PartialEq)]
pub enum LineAxis {
    XAxis,
    YAxis,
}

/**
  A line represented as a starting point and a slope. To avoid issues with vertical
  lines, any line with a slope larger than 1 is represented as a slope relative to the
  y-axis instead

  Additionally, The start point is always the lowest point along the primary axis

  Example of a line with a slope higher than one
  ```
  /*
  ^
  |      *  ^
  |     *   |
  |    *    |  length_along_primary
  |   *     |
  |  *      v
  |
  +----------------->
  */
  ```
*/
#[derive(Debug, Clone, PartialEq)]
pub struct SlopeLine {
    pub start: Point2<f32>,
    pub slope: f32,
    pub length_along_primary: f32,
    pub axis: LineAxis,
}



#[cfg(test)]
mod util_function_tests {
    use super::*;

    #[test]
    fn distance_between_pos_x_and_origin_is_correct() {
        let point = Point3::new(0., 0., 0.);
        let normal = Vector3::new(-1., 0., 0.);
        let pos = Point3::new(1., 0., 0.);

        assert_eq!(plane_distance(point, normal, pos), 1.);
    }

    #[test]
    fn distance_between_neg_y_and_other_point_is_correct() {
        let point = Point3::new(0.5, 0.5, 0.);
        let normal = Vector3::new(0., 1., 0.);
        let pos = Point3::new(0., -1., 0.);

        assert_eq!(plane_distance(point, normal, pos), 1.5);
    }
    #[test]
    fn distance_between_neg_y_and_far_away_point_is_correct() {
        let point = Point3::new(0.5, -3., 0.);
        let normal = Vector3::new(0., 1., 0.);
        let pos = Point3::new(0., -1., 0.);

        assert_eq!(plane_distance(point, normal, pos), -2.);
    }

    #[test]
    fn distance_between_planes_at_different_location_works() {
        let point = Point3::new(0.5, 0.5, 0.);
        let normal = Vector3::new(0., 1., 0.);
        let pos = Point3::new(0., -0.2, 0.);

        assert_eq!(plane_distance(point, normal, pos), 0.7);
    }


    #[test]
    fn clip_to_x_changes_nothing_when_fully_above_plane() {
        let line = (Point3::new(0.5, -0.5, 0.5), Point3::new(0.25, 0., 0.3));

        let normal = Vector3::new(-1., 0., 0.);
        let pos = Point3::new(1., 0., 0.);

        assert_eq!(clip_line_to_plane(line, normal, pos), Some(line));
    }
    #[test]
    fn clip_to_x_clips_when_fully_on_below_plane() {
        let line = (Point3::new(1.5, -0.5, 0.5), Point3::new(1.25, 0., 0.3));

        let normal = Vector3::new(-1., 0., 0.);
        let pos = Point3::new(1., 0., 0.);
        assert_eq!(clip_line_to_plane(line, normal, pos), None);
    }
    #[test]
    fn clip_to_y_when_start_is_below_plane() {
        let line = (Point3::new(0., 2., 0.), Point3::new(0., 0., 0.));

        let normal = Vector3::new(0., -1., 0.);
        let pos = Point3::new(0., 1., 0.);

        let expected = Some((Point3::new(0., 1., 0.), Point3::new(0., 0., 0.)));
        assert_eq!(clip_line_to_plane(line, normal, pos), expected);
    }
    #[test]
    fn clip_to_neg_z_when_end_is_below_plane() {
        let line = (Point3::new(0., 0., 0.), Point3::new(-2., -2., -2.));

        let expected = Some((Point3::new(0., 0., 0.), Point3::new(-1., -1., -1.)));

        let normal = Vector3::new(0., 0., 1.);
        let pos = Point3::new(0., 0., -1.);
        assert_eq!(clip_line_to_plane(line, normal, pos), expected);
    }
}


#[cfg(test)]
mod slope_tests {
    use super::*;

    #[test]
    fn line_along_x_axis_has_correct_values() {
        let line = Line2d::new(Point2::new(10., 5.), Point2::new(20., 5.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(10., 5.),
            slope: 0.,
            length_along_primary: 10.,
            axis: LineAxis::XAxis,
        });
    }

    #[test]
    fn line_along_y_axis_has_correct_values() {
        let line = Line2d::new(Point2::new(10., 5.), Point2::new(10., 15.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(5., 10.),
            slope: 0.,
            length_along_primary: 10.,
            axis: LineAxis::YAxis,
        });
    }

    #[test]
    fn line_along_y_with_inverted_start_axis_has_correct_values() {
        let line = Line2d::new(Point2::new(10., 15.), Point2::new(10., 5.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(5., 10.),
            slope: 0.,
            length_along_primary: 10.,
            axis: LineAxis::YAxis,
        });
    }

    #[test]
    fn line_slope_is_calculated_correctly() {
        let line = Line2d::new(Point2::new(10., 5.), Point2::new(20., 10.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(10., 5.),
            slope: 0.5,
            length_along_primary: 10.,
            axis: LineAxis::XAxis,
        });
    }

    #[test]
    fn line_slope_is_calculated_correctly_for_inverted_lines() {
        let line = Line2d::new(Point2::new(10., 20.), Point2::new(5., 10.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(10., 5.),
            slope: 0.5,
            length_along_primary: 10.,
            axis: LineAxis::YAxis,
        });
    }

    #[test]
    fn line_slope_is_calculated_correctly_for_inverted_negative_slope_lines() {
        let line = Line2d::new(Point2::new(4., 1.), Point2::new(3., 5.));
        let sloped = line.to_sloped();
        assert_eq!(sloped, SlopeLine{
            start: Point2::new(1., 4.),
            slope: -0.25,
            length_along_primary: 4.,
            axis: LineAxis::YAxis,
        });
    }
}
